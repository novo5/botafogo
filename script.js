window.onload = () => {
    informacaoJogadores()
}

let cards = document.querySelector('.cards')

const informacaoJogadores = () => {
    fetch('jogadores.json')
    .then(resposta => resposta.json())
    .then(dados => dados.forEach((jogador, indice) => {

    let card = document.createElement('div')
    card.classList.add('card')
    cards.appendChild(card)

    card.innerHTML = `
    <img class="foto" src=${jogador.imagem} alt="foto do jogador"/>
    <h2 class="nome">${jogador.nome}</h2>
    <p class="posicao">${jogador.posicao}</p>
    `
    
    }
    ))
}

const inputJogador = document.querySelector('#jogador')

const hideCard = (cardJogadores, inputValue) => {
    cardJogadores
    .filter(card => !card.textContent.toLowerCase().includes(inputValue)) 
    .forEach(card => {
        card.classList.remove('d-flex')
        card.classList.add('hidden')
    })
}

const mostraCard = (cardJogadores, inputValue) => {
    cardJogadores
    .filter(card => card.textContent.toLowerCase().includes(inputValue)) 
    .forEach(card => {
        card.classList.remove('hidden')
        card.classList.add('d-flex')
    })
}

inputJogador.addEventListener('input', event => {
    const inputValue = event.target.value.trim().toLowerCase()
    const cardJogadores =  Array.from(cards.children)

    hideCard(cardJogadores, inputValue)
    mostraCard(cardJogadores, inputValue)
})


const cookieJogador = () => {
    document.cookie += "jogador=" + [2];
    document.cookie += "jogador=" + [4];
    document.cookie +="jogador=" + [5];
    document.cookie += "jogador=" + [6];

    window.location.href = 'pagina_jogador.html'; 

}


